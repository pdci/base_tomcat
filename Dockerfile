#https://github.com/CentOS/CentOS-Dockerfiles
#https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
FROM centos:centos7 as production
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor="COTEC"
LABEL Description="Centos 7 , PHP 7.2, composer"
LABEL License=GPLv2
LABEL Version=0.0.1
LABEL Features="1 melhoria x meria y"
LABEL tag=""

#WORKDIR /opt/

# Install prepare infrastructure
RUN yum -y update && \
 yum -y install wget && \
 yum -y install tar

# Prepare environment
ENV JAVA_HOME /opt/java
ENV CATALINA_HOME /opt/tomcat
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin:$CATALINA_HOME/scripts

# Install Oracle Java8
ENV JAVA_VERSION 8u191
ENV JAVA_BUILD 8u191-b12
ENV JAVA_DL_HASH 2787e4a523244c269598db4e85c51e0c

RUN wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" \
 http://download.oracle.com/otn-pub/java/jdk/${JAVA_BUILD}/${JAVA_DL_HASH}/jdk-${JAVA_VERSION}-linux-x64.tar.gz && \
 tar -xvf jdk-${JAVA_VERSION}-linux-x64.tar.gz && \
 rm jdk*.tar.gz && \
 mv jdk* ${JAVA_HOME}


# Install Tomcat
ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.5.35

RUN wget http://mirror.linux-ia64.org/apache/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
 tar -xvf apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
 rm apache-tomcat*.tar.gz && \
 mv apache-tomcat* ${CATALINA_HOME}

RUN chmod +x ${CATALINA_HOME}/bin/*sh

# Create Tomcat admin user
ADD create_admin_user.sh $CATALINA_HOME/scripts/create_admin_user.sh
ADD tomcat.sh $CATALINA_HOME/scripts/tomcat.sh
RUN chmod +x $CATALINA_HOME/scripts/*.sh

# Create tomcat user
RUN groupadd -r tomcat && \
 useradd -g tomcat -d ${CATALINA_HOME} -s /sbin/nologin  -c "Tomcat user" tomcat && \
 chown -R tomcat:tomcat ${CATALINA_HOME}

WORKDIR /opt/tomcat

EXPOSE 8080
EXPOSE 8009
COPY conf/context.xml  /opt/tomcat/webapps/manager/META-INF/context.xml

#ENV JAVA_OPTS="-Dfile.encoding=UTF-8 -server -Xms2048m -Xmx4096m -XX:NewSize=512m -XX:MaxNewSize=512m -XX:+DisableExplicitGC -DproxyHost=proxy.icmbio.gov.br -DproxyPort=8080"
#2.2.2. Configurar JAVA_OPTS
#RUN echo 'JAVA_OPTS="-Dfile.encoding=UTF-8 -server -Xms2048m -Xmx4096m -XX:NewSize=512m -XX:MaxNewSize=512m -XX:+DisableExplicitGC -DproxyHost=proxy.icmbio.gov.br -DproxyPort=8080"' >>  /opt/apache-tomcat-7.0.78/bin/catalina.sh
#a) vi /opt/apache-tomcat-7.0.78/bin/catalina.sh
#b) Adicionar o texto na linha abaixo da linha: "#!/bin/sh".

#OBS: Toda a instrução acima deverá ficar na mesma linha, sem quebras.

##2.2.3. Iniciar Tomcat
#a) /opt/apache-tomcat-7.0.78/bin/startup.sh
#b) Verificar se o Tomcat está executando
#tailf -1000 /opt/apache-tomcat-7.0.78/logs/catalina.out
#c) Aguardar até aparecer: INFO: Server startup in XXXXX, dentre as 10 ultimas
#no terminal
#d) acessar via browser: http://<ip-maquina>:8080

#2.2.4. Parar Tomcat
#a) /opt/apache-tomcat-7.0.78/bin/shutdown.sh

#2.2.5. Arquivos .war
#
#a) cd /opt/apache-tomcat-7.0.78/webapps
#b) https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/war/salve.war

#2.2.6. Arquivos de Configuração  ( /data/salve )
#1.	mkdir -p /data/salve/arquivos
#2.	mkdir -p /data/salve/temp
#3.	mkdir -p /data/salve/anexos
#4.	mkdir -p /data/salve/planilhas
#5.	mkdir -p /data/salve/multimidia
#6.	mkdir -p /data/salve/logs
#7.	chmod -R 0775 /data/salve
#8.	cd /data/salve
#9.	https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/config_prd.properties
# config.properties

#10.	editar o arquivo config.properties e colocar os dados de conexão com o banco de dados postgres para acesso em produção, linhas:
#5.1) vi /data/salve/config.properties
#a) configurar a linha: dataSource.url=jdbc:postgresql://<ip-postgres>/<banco-dados>
#ex: dataSource.url=jdbc:postgresql://10.197.93.14/db_dev_cotec
#b) dataSource.username=usr_salve
#c) dataSource.password=<password>
#ex: dataSource.password=123456XPTO

#2.2.7 – Baixar o Manual do Sistema e demais arquivos do sistema
#a) cd /data/salve/arquivos
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/manual_salve.pdf
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/planilha_modelo_importacao_registros_salve.zip
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/arvoreAcoesConservacao.pdf
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/arvoreAmeacas.pdf
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/arvoreUsos.pdf
#https://svn.icmbio.gov.br/svn/salve/outs/salve/implementacao/data/arquivos/no-image.jpeg


#2.2.8 – Iniciar o Tomcat
#/opt/apache-tomcat-7.0.78/bin/startup.sh
USER tomcat
CMD ["tomcat.sh"]

#
#FROM production as testing
##MAINTAINER ICMBIO <roquebrasilia@gmail.com>
#LABEL Vendor="COTEC"
#LABEL Description="Centos 7 , PHP 7.2, composer , npm,  yarn"
#LABEL License=GPLv2
#LABEL Version=0.0.1
#ENV PDCI_SONARQUBE_URL_PORT=${PDCI_SONARQUBE_URL_PORT:-http://localhost:9000}
### Instalacao do node para os testes
##RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -  && \
##     yum install -y --setopt=tsflags=nodocs lynx net-tools git maven ant wget automake autoconf  dh-autoreconf nasm libjpeg-turbo-utils  php-pecl-xdebug  php-cli  pngquant nodejs && \
##     yum clean all && \
##     rm -rf /var/cache/yum && \
##     npm  install -g  yarn
#
#WORKDIR /opt/
#
#RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip && \
#    unzip sonar-scanner-cli-3.2.0.1227-linux.zip && \
#    rm -f sonar-scanner-cli-3.2.0.1227-linux.zip && \
#    export PATH="$PATH:/opt/sonar-scanner-3.2.0.1227-linux/bin"
#
#COPY conf/sonar-scanner.properties  /opt/sonar-scanner-3.2.0.1227-linux/conf
#
#WORKDIR $APP_HOME
#
#RUN composer install && \
#  yarn install
#
#CMD ["/run-httpd.sh"]

